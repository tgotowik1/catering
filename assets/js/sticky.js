//////// STICKY NAVVBAR + LOGO APPEAR ///////////
$(document).ready(function () {
  var navbar = document.getElementById("navbar");
  var logo = document.getElementById("logo_sticky");
  var startcontent = document.getElementById("start");
  var sticky = navbar.offsetTop;

  function stickyNavbar() {
    if (window.pageYOffset >= sticky) {
      // add sticky class
      navbar.classList.add("sticky");
      // fixed header + content
      startcontent.style.paddingTop = navbar.offsetHeight + 50 + "px";
      // show sticky logo only >=768px
      if (window.innerWidth >= 768) {
        logo.style.display = "block";
      } else {
        logo.style.display = "none";
      }
    } else {
      // remove sticky class
      navbar.classList.remove("sticky");
      // set pading back to normal
      startcontent.style.paddingTop = "50px";
      // set sticky logo to none
      logo.style.display = "none";
    }
  }

  window.addEventListener("resize", function () {
    stickyNavbar();
  });
  window.addEventListener("scroll", function () {
    stickyNavbar();
  });
});
