<?php

// if the url field is empty, but the message field isn't
if(isset($_POST['url']) && $_POST['url'] == '' && $_POST['message'] != ''){

	$youremail = 'info@threesixty-catering.com';

	$body = "Eine neue Mail von threesixty-catering.com:
	Betreff:  $_POST[subject]
	Name:  $_POST[name]
	E-Mail: $_POST[email]
	Telefon: $_POST[telephone]
	Message: $_POST[message]";

	if( $_POST['email'] && !preg_match( "/[\r\n]/", $_POST['email']) ) {
	  $headers = "From: $_POST[email]";
	} else {
	  $headers = "From: $youremail";
	}

	mail($youremail, 'Contact Form', $body, $headers );

	echo "Mail wurde gesendet. Vielen Dank. Wir werden uns sobald wie m&ouml;glich zur&uuml;ckmelden! <br>";
	echo "Zur&uuml;ck zu <a href='/contact.html'>https://www.threesixty-catering.com/contact.html</a>";

}
else {
	echo "Etwas ist schief gelaufen. Probiere es nochmal. <br>";
	echo "Zur&uuml;ck zu <a href='/contact.html'>https://www.threesixty-catering.com/contact.html</a>";
}

?>